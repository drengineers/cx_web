var cxApp = angular.module('cxApp', ['ui.router','ngAnimate','ngCookies','ngSanitize','ionic','chart.js','ion-sticky']);

cxApp.run(function($rootScope, $state,$ionicPopup, $stateParams,$ionicPlatform,$ionicLoading,$sce,$cookieStore,$http) {
	 $ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);}
		if(window.StatusBar) {StatusBar.styleDefault();}
	 });
    $rootScope.version = 'AVAYA';
    $rootScope.versionNumber = '6.1.4';
    $rootScope.color = [];
    $rootScope.labels = [];
    $rootScope.data = [];
    $rootScope.pollData = {};
    $rootScope.cookieNatifction = [];
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.comeback =function(){history.back();};
    $rootScope.headerTitle = "Header";
	$rootScope.headershowhide = true;
    $rootScope.isPassWord = localStorage.isPassWord?localStorage.isPassWord:false;
    $rootScope.domain = 'https://6-2-dot-cx-app-stage.appspot.com';
    $rootScope.files ='https://6-2-dot-cx-app-stage.appspot.com/files';
	$rootScope.imgUrl ='https://6-2-dot-cx-app-stage.appspot.com/files/';
    $rootScope.apiUrl ='/m';
    $rootScope.iframeUrl = '';
    $rootScope.meetingData = null;
    $rootScope.headerBlockOrNone = false;
    // $rootScope.showRightMenu=true;
    $rootScope.getBase64Image =function(base64string) {return base64string.replace(/^data:image\/(png|jpg);base64,/, "");};
	$rootScope.timeConversion = function(time){
        time +=new Date().getTimezoneOffset() * 60 * 1000;
		time = time/1000;
		var toTime='';
		if(time < 60){
			toTime= 0+'m';
		}else if(time > 60 && time < 3600){
			toTime = Math.round(time/60).toFixed(0)+'m';
		}else if(time > 3600 && time < 86400){
			toTime = Math.round(time/3600).toFixed(0)+'h';
		}else if(time > 86400 && time < 2592000){
			toTime = Math.round(time/86400).toFixed(0)+'d';
		}else{
			toTime = Math.round(time/2592000).toFixed(0)+'M';
		}
		return toTime;
	};
	$rootScope.judgeHttp =function(val){
		if(val=='' || val==null){return val;}
		var Expression=/^http(s)?:\/\//;
		var objExp=new RegExp(Expression);
		if(objExp.test(val) != true){return $rootScope.imgUrl+val;} else {return val;}
	};
	$rootScope.letterConversion = function (letter) {
        var newarr,newarr1=[];
        newarr = letter . toLowerCase() . split(" ");
        for(var i = 0 ; i < newarr . length ; i++){
            newarr1 . push(newarr[i][0] . toUpperCase()+newarr[i] . substring(1));
        }
        return newarr1.join(' ');
    };
    $rootScope.conversionActivity = function (val){
       if(val == "uMessage" || val == "uImage"){
           return "Attendee";
	   }else if(val == "mMessage" || val == "mVideo" || val == "mImage"){
           return "Managed";
	   }else if(val == "twitter"){
           return "Twitter";
	   }else if(val == "instagram"){
           return "Instagram";
       }
    };
	$rootScope.URL = '/(https?://|ftps?://)?((d{1,3}.d{1,3}.d{1,3}.d{1,3})(:[0-9]+)?|([w]+.)(S+)(w{2,4})(:[0-9]+)?)(/?([w#!:.?+=&%@!-/]+))?/ig';
	$rootScope.urlReplace =function(val){
		return val.replace(/(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)/g, "<a href='$1$2' target='_blank' class='changeColor'>$1$2</a>");
	};
	$rootScope.deliberatelyTrustDangerousSnippet = function(content) {
		return $sce.trustAsHtml(content);
	};
    $rootScope.showConfirm = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Log Out',
            template: 'Are you sure you want to log out?'
        });
        confirmPopup.then(function(res) {
            if(res) {
                console.log('You are sure');
                localStorage.clear();
                $state.go('login');
            } else {
                console.log('You are not sure');
            }
        });
    };
    $rootScope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Your Profile has been updated!',
            template: ''
        });
        alertPopup.then(function(res) {
            console.log('Thank you for not eating my delicious ice cream cone');
        });
    };

    $rootScope.showAlertNotification = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'You have already completed this survery.',
            template: ''
        });
        alertPopup.then(function(res) {
            console.log('You have already completed this survery.');
        });
    };

    $rootScope.getAppConfig =function () {
        var url =$rootScope.apiUrl+'/config/';
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
           localStorage.terms =JSON.stringify(response.data.terms);
           localStorage.support = JSON.stringify(response.data.support);
           $rootScope.mailto = JSON.parse(localStorage.support).mailto!=''?JSON.parse(localStorage.support).mailto:'support@cxapp.io';
           $rootScope.subject = JSON.parse(localStorage.support).subject;
           $rootScope.footerDate = JSON.parse(localStorage.terms).copyright.text;
           $rootScope.footerDateUrl = JSON.parse(localStorage.terms).copyright.link;
           $rootScope.footerCenter = JSON.parse(localStorage.terms).title;
           $rootScope.footerCenterUrl = JSON.parse(localStorage.terms).link;
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };
    $rootScope.getAppConfig();
	
});
cxApp.config(function($stateProvider, $urlRouterProvider,$qProvider,$httpProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    $urlRouterProvider.otherwise('/welcome');
    $stateProvider
    	.state('welcome',{
			url:'/welcome',
			views:{
				'':{
					templateUrl:'tpls/welcome/welcome.html',
					controller:'welcomeController'
				 }

			}
		})
    
    	.state('index',{
			url:'/index',
			views:{
				'':{

					templateUrl:'tpls/home/home-view.html'
				},
				'header':{
					templateUrl:'tpls/public/header.html',
                    controller:'homeController'
				},
				
				'home@index':{
					templateUrl:'tpls/home/home.html',
					controller:'homeController'
				},
				'footer@index':{
					templateUrl:'tpls/public/footer.html',
                    controller:'homeController'
				},
			},
            cache:false
		})

    	.state('homeLists',{
			url:'/homeLists',
			views:{
				'':{
					templateUrl:'tpls/homeLists/home-lists-view.html'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				},
			},
            cache:false
		})
    	
    	.state('homeLists.agenda',{
			url:'/agenda',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/agenda.html',
					controller:'agendaController'
				},
			},
            cache:false
		})

    	.state('homeLists.attendees',{
			url:'/attendees',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/attendees.html',
					controller:'attendeesController'
				}
			},
            cache:false
		})
        .state('homeLists.myagenda',{
            url:'/myagenda',
            views:{
                'main@homeLists':{
                    templateUrl:'tpls/homeLists/myagenda.html',
                    controller:'agendaDetailController'
                },
            }
        })
    	.state('email',{
			url:'/email',
			views:{
				'':{
					templateUrl:'tpls/homeLists/attendees/attendees-email.html',
					controller:'attendees_emailController'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				}
			}
		})
    	
    	.state('homeLists.econtent',{
			url:'/econtent',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/econtent.html',
					controller:'econtentController'
				}
			},
            cache:false
		})
    	
    	.state('homeLists.travelLogistics',{
			url:'/travelLogistics',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/travelLogistics.html',
					controller:'travelLogisticsController'
				}
			},
            cache:false

		})
    	.state('homeLists.wayfiding',{
			url:'/wayfiding',
			views:{
				'main@homeLists':{
					//templateUrl:'tpls/homeLists/wayfiding.html',
					controller:'wayfidingController'
				}
			}
		})
    	.state('homeLists.notifications',{
			url:'/notifications',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/notifications.html',
					controller:'notificationsController'
				}
			},
            cache:false
		})
	
    	.state('lists',{
            url: '/lists',
            views:{
            	'':{
		            templateUrl: 'tpls/member/lists.html',
					controller:'listsController'
	            },
            },
            cache:false
   		 })
    	.state('login',{
            url: '/login',
            views:{
            	'':{
		            templateUrl: 'tpls/member/login.html',
		            controller:'loginEmailController'
	            }
            }
   		 })
    	.state('loginPassword',{
			url: '/loginPassword',
			views:{
			   '':{
					templateUrl: 'tpls/member/loginPassword.html',
					controller:'loginPasswordController'
			   }
			}
		})
        .state('release',{
            url: '/release',
            views:{
                '':{
                    templateUrl: 'tpls/public/release.html',
                    controller:'releaseController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html'
                },

            }
        })
    	.state('complete',{
            url: '/complete',
            views:{
            	'':{
		            templateUrl: 'tpls/public/complete.html',
					controller:'completeController'
	            }
            }
   		 })
        .state('completeyourprofile',{
            url: '/completeyourprofile.',
            views:{
                '':{
                    templateUrl: 'tpls/public/completeyourprofile.html',
                    controller:'completeController'
                },
            }
        })
    	.state('profile',{
            url: '/profile/:id',
            views:{
            	'':{
		            templateUrl: 'tpls/member/profile.html',
					controller:'profileController'
	            },
	            'header':{
					templateUrl:'tpls/public/header.html'
				},
	            
            },
            cache:false
   		 })

        .state('updatepassword',{
            url: '/updatepassword',
            views:{
                '':{
                    templateUrl: 'tpls/member/preferences/updatepassword.html',
                    controller:'updatepasswordController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html',
                    controller:'updatepasswordController'
                }
            },
            cache:false
        })
        .state('bio',{
            url: '/bio',
            views:{
                '':{
                    templateUrl: 'tpls/member/preferences/bio.html',
					controller:'preferencesController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html'
                },

            },
            cache:false
        })
        .state('preferences',{
            url: '/preferences',
            views:{
                '':{
                    templateUrl: 'tpls/member/preferences.html',
                    controller:'preferencesController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html'
                },

            },
            cache:false
        })

     	.state('map',{
            url: '/map',
            views:{
            		'':{
		            templateUrl: 'tpls/homeLists/travelLogistics/map.html',
					controller:'mapController'
	            },
	            'header':{
					templateUrl:'tpls/public/header.html'
				},
	            
            }
    	})
		.state('mapto',{
			url: '/mapto',
			views:{
				'':{
					templateUrl: 'tpls/homeLists/travelLogistics/mapto.html',
					controller:'maptoController'
				},
				 'header':{
					templateUrl:'tpls/public/header.html'
				},
			}
		})
	 	.state('comment',{
			url: '/comment',
			views:{
				'':{
					templateUrl: 'tpls/homeLists/comment.html',
					controller:'commentController'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				},
			},
            cache:false
		})
		.state('eject',{
			url: '/eject',
			views:{
				'':{
					templateUrl: 'tpls/public/eject.html',
					controller:'ejectController'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				},
			}
		})
        .state('poll',{
            url: '/poll',
            views:{
                '':{
                    templateUrl: 'tpls/homeLists/poll/poll.html',
                    controller:'pollController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html'
                },
            }
        })
        .state('poll-results',{
            url: '/poll-results',
            views:{
                '':{
                    templateUrl: 'tpls/homeLists/poll/pollResults.html',
                    controller:'pollController'
                },
                'header':{
                    templateUrl:'tpls/public/header.html'
                },
            }
        })


});
cxApp.service('cache',function($cacheFactory){
    return $cacheFactory('localCache');
});
cxApp.factory('locals', ['$window', function ($window) {
    return {        //存储单个属性
        set: function (key, value) {
            $window.localStorage[key] = value;
        },        //读取单个属性
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },        //存储对象，以JSON格式存储
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);//将对象以字符串保存
        },        //读取对象
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');//获取字符串并解析成对象
        }

    };
}]);



