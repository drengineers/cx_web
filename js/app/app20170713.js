var cxApp = angular.module('cxApp', ['ui.router','ngAnimate','ionic']);

cxApp.run(function($rootScope, $state, $stateParams,$ionicPlatform,$ionicLoading) {
	 $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.comeback =function(){
        //$rootScope.$ionicGoBack();
         history.back();
    };
    $rootScope.headerTitle = "Header";
	$rootScope.headershowhide = true;
});
cxApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/welcome');
    $stateProvider
    	.state('welcome',{
			url:'/welcome',
			views:{
				'':{
					templateUrl:'tpls/welcome/welcome.html',
					controller:'welcomeController'
				 }

			}
		})
    
    	.state('index',{
			url:'/index',
			views:{
				'':{
					templateUrl:'tpls/home/home-view.html'
				 },
				'header':{
					templateUrl:'tpls/public/header.html'
				},
				
				'home@index':{
					templateUrl:'tpls/home/home.html',
					controller:'homeController'
				},
				'footer@index':{
					templateUrl:'tpls/public/footer.html',
					controller:'footerController'
				}
			}
		})

    	.state('homeLists',{
			url:'/homeLists',
			views:{
				'':{
					templateUrl:'tpls/homeLists/home-lists-view.html'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				}
			}
		})
    	.state('homeLists.slides',{
			url:'/slides',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/home/slides.html',
					controller:'slidesController'
				}
			}
		})
    	.state('homeLists.slides2',{
			url:'/slides',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/home/slides-2.html',
					controller:'slides2Controller'
				}
			}
		})
    	.state('homeLists.slides3',{
			url:'/slides',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/home/slides-3.html',
					controller:'slides3Controller'
				}
			}
		})
    	
    	.state('homeLists.agenda',{
			url:'/agenda',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/agenda.html',
					controller:'agendaController'
				}
			}
		})

    	.state('homeLists.attendees',{
			url:'/attendees',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/attendees.html',
					controller:'attendeesController'
				}
			}
		})
    	.state('email',{
			url:'/email',
			views:{
				'':{
					templateUrl:'tpls/homeLists/attendees/attendees-email.html',
					controller:'attendees_emailController'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				}
			}
		})
    	
    	.state('homeLists.econtent',{
			url:'/econtent',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/econtent.html',
					controller:'econtentController'
				}
			}
		})
    	
    	.state('homeLists.travelLogistics',{
			url:'/travelLogistics',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/travelLogistics.html',
					controller:'travelLogisticsController'
				}
			}
		})
    	.state('homeLists.wayfiding',{
			url:'/wayfiding',
			views:{
				'main@homeLists':{
					//templateUrl:'tpls/homeLists/wayfiding.html',
					controller:'wayfidingController'
				}
			}
		})
    	.state('homeLists.notifications',{
			url:'/notifications',
			views:{
				'main@homeLists':{
					templateUrl:'tpls/homeLists/notifications.html',
					controller:'notificationsController'
				}
			}
		})
		.state('lists',{
            url: '/lists',
            views:{
            	'':{
		            templateUrl: 'tpls/member/lists.html',
					controller:'listsController'
	            }
            }
   		 })
    	.state('login',{
            url: '/login',
            views:{
            	'':{
		            templateUrl: 'tpls/member/login.html',
		            controller:'loginEmailController'
	            }
            }
    	})
    	.state('loginPassword',{
            url: '/loginPassword',
            views:{
	           '':{
	           		templateUrl: 'tpls/member/loginPassword.html',
	           		 controller:'loginPasswordController'
	           }
	        } 
    	})
    	.state('release',{
            url: '/release',
            views:{
            		'':{
						templateUrl: 'tpls/public/release.html',
						controller:'listsController'
					},
					'header':{
						templateUrl:'tpls/public/header.html'
					}
	            
            }
   		})
		.state('profile',{
			url:'/profile',
			view:{
				'':{
					templateUrl:'tpls/member/profile.html',
					controller:'profileController'
				},
				'header':{
					templateUrl:'tpls/public/header.html'
				}
			}
		})


});


