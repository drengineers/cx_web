cxApp
.controller('homeController', function($scope,cache,$rootScope,$stateParams, $ionicLoading,$ionicLoadingConfig,$timeout,$ionicSlideBoxDelegate,$state,$http,$ionicScrollDelegate,$sce) {
    localStorage.headerTitle = $rootScope.version=='HPE'?'Briefing Overview':'Meeting Overview';
    $rootScope.headerTitle = localStorage.headerTitle;
    // localStorage.showRightMenu = true;
    // $rootScope.showRightMenu =localStorage.showRightMenu;
    $rootScope.email = localStorage.loginEmail;
    $scope.includeActivityStream = true;
    $rootScope.meetingData = JSON.parse(localStorage.meetingAll);
    $scope.acNum =0 ;
    $scope.tag = "";
    $scope.hasmore = false;
    $scope.page = 0;
    $scope.bar=[
        {
            name:'All',
            icon:'stream_all',
            tag:0
        }
    ];
    $scope.getAllFeed = function (index,pageSize,types) {
        var url =$rootScope.apiUrl+'/feeds/?meetingId='+localStorage.selectmeetingId;
        if(index){
            url +='&index='+index;
        }
        if(pageSize){
            url +='&pageSize='+pageSize;
        }
        if(types){
            url +='&types='+types;
        }
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if(response.data.index==0){
                localStorage.feed = JSON.stringify(response.data.feeds);
                $scope.feed = response.data.feeds;
            }else{
                $scope.feed = JSON.parse(localStorage.feed);
            }

            if(response.data.feeds.length < response.data.pageSize){
                $scope.hasmore = false;
            }else{
                $scope.hasmore = true;
            }
            $scope.dataEach(response.data);
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    $scope.dataEach =function (data) {
        var dateCurrentToUTCS =  new Date(new Date().toUTCString()).getTime();
        angular.forEach(data.feeds, function(value){
            value.timestamp = new Date(value.timestamp).getTime();
            var time = (dateCurrentToUTCS-value.timestamp);
            value.times = $rootScope.timeConversion(time);
            value.typeLetter = $rootScope.conversionActivity( value.type);
            value.thumbnail = $rootScope.judgeHttp(value.thumbnail);
            value.assetUrl = $rootScope.judgeHttp(value.assetUrl);
            value.content = $rootScope.urlReplace(value.content);
            value.targetUrl = $sce.trustAsResourceUrl( value.targetUrl);
            if(value.comments.length != 0){
                angular.forEach(value.comments, function(value){
                    value.thumbnail = $rootScope.judgeHttp(value.thumbnail);
                });
            }
            value.isLike = false;
            value.likers.forEach(function (t) {
                if(t == $rootScope.email){
                    value.isLike = true;
                }
            });
            if(data.index > 0){
                $scope.feed.push(value);
            }
            localStorage.feed = JSON.stringify($scope.feed);
        });

        $timeout(function () {
            if(data.index > 0){
                localStorage.feed = JSON.stringify($scope.feed);
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $ionicLoading.hide();
        }, 1000);
    };

    $scope.addBar =function () {
        var select =JSON.parse(localStorage.meetingSelect);
        if(select.feedStatus.managed){
            $scope.bar.push(
                {
                    name:'MANAGED',
                    icon:'stream_managed',
                    tag:1
                }
            );
        }
        if(select.feedStatus.attendee){
            $scope.bar.push(
                {
                    name:'ATTENDEE',
                    icon:'stream_attendee',
                    tag:2
                }
            );
        }
        if(select.feedStatus.news){
            if(select.feedStatus.twitter && select.feedStatus.instagram){
                $scope.bar.push(
                    {
                        name:'SOCIAL',
                        icon:'stream_social',
                        tag:7
                    }
                );
            }else if(select.twitter){
                $scope.bar.push(
                    {
                        name:'SOCIAL',
                        icon:'stream_social',
                        tag:3
                    }
                );
            }else if(select.feedStatus.instagram){
                $scope.bar.push(
                    {
                        name:'SOCIAL',
                        icon:'stream_social',
                        tag:4
                    }
                );
            }
            $scope.bar.splice(1, 0, {
                name:'NEWS',
                icon:'news',
                tag:6
            });
        }else{
            if(select.feedStatus.twitter){
                $scope.bar.push(
                    {
                        name:'TWITTER',
                        icon:'stream_twitter',
                        tag:3
                    }
                );
            }
            if(select.feedStatus.instagram){
                $scope.bar.push(
                    {
                        name:'INSTAGRAM',
                        icon:'stream_instagram',
                        tag:4
                    }
                );
            }
        }
        if(select.feedStatus.slack){
            if(select.slackButton.icon!='' && select.slackButton.label!='' && select.slackButton.link!=''){
                $scope.bar.push(
                    {
                        name:select.slackButton.label,
                        icon:"stream_slack",
                        iconUrl:$rootScope.imgUrl+select.slackButton.icon,
                        link:select.slackButton.link,
                        tag:5
                    }
                );
            }
        }
        console.log($scope.bar);
        $scope.w = 100/$scope.bar.length;
        $scope.widthFooterBar = {
            "width" : $scope.w+'%'
        };
        $scope.includeActivityStream = select.includeActivityStream;
        $scope.displayTravelAndLogisitics =select.displayTravelAndLogisitics;
    };

    $scope.getMeeting =function(){
        var url =$rootScope.apiUrl+'/meeting/?meetingId='+localStorage.selectmeetingId;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            angular.forEach(response.data.banners,function (val) {
                if(val.image=='' || val.image==null ){
                    val.image = '';
                }else{
                    val.image =$rootScope.imgUrl+val.image;
                }
            });
            localStorage.meetingSelect = JSON.stringify(response.data);
            $scope.meetingSelect =JSON.parse(localStorage.meetingSelect);
            console.log( $scope.meetingSelect);

            $ionicSlideBoxDelegate.update();
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    $timeout(function () {
      $scope.getMeeting();
    },200);

    $timeout(function () {
        $scope.getAllFeed(0);
    }, 500);

    $timeout(function () {
        $scope.addBar();
    }, 1000);

    $scope.urlClick = function (val,title) {
        localStorage.iframeTitle = title;
        localStorage.iframeUrl = val;
        $state.go('eject');
    };

    $scope.typeRe = function (val) {
        var type='';
        switch (val){
            case 1:
                type = "mMessage,mVideo,mImage";
                break;
            case 2:
                type = "uMessage,uImage";
                break;
            case 3:
                type = "twitter";
                break;
            case 4:
                type = "instagram";
                break;
            case 5:
                type = "slack";
                break;
            case 6:
                type = "news";
                break;
            case 7:
                type = "twitter,instagram";
                break;
            default:
                type = "";
        }
        return type;
    };

    $scope.doRefresh = function(tag) {
        $scope.getAllFeed(0,20,$scope.typeRe(tag));
        $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.addActive = function (tag,num) {
        $scope.acNum =num;
        $scope.tag = tag;
        $ionicLoading.show();
        $scope.getAllFeed(0,20,$scope.typeRe(tag));
        $ionicScrollDelegate.scrollTop([true]);
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    };

    $scope.loadMore=function (tag) {
        $ionicLoading.show();
        $scope.page = $scope.page+1;
        $scope.getAllFeed($scope.page,20,$scope.typeRe(tag));
    };

    $scope.addLike = function(id,type){
        var url =$rootScope.apiUrl+'/like/?feedId='+id+'&type='+type+'&useremail='+localStorage.loginEmail;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        console.log(url);
        $http({
            method: 'POST',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            if(response.data.type==1){
                $scope.feed.forEach(function (t) {
                    if(t._id == id){
                        t.likers.push($rootScope.email);
                        t.numLike = t.numLike + 1;
                        t.isLike = true;
                    }
                })
            }else{
                $scope.feed.forEach(function (t) {
                    if(t._id == id){
                        var arrLike = [];
                        t.likers.forEach(function (t2) {
                            if(t2 != $rootScope.email){
                                arrLike.push(t2);
                            }
                        });
                        t.likers = arrLike;
                        t.numLike = t.numLike - 1;
                        t.isLike = false;
                    }
                })
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });

    };

    $timeout(function () {
        $ionicSlideBoxDelegate.update();
        $ionicSlideBoxDelegate.$getByHandle("box").loop(true);
    }, 2000);


    $scope.goComment =function (id) {
        localStorage.commentId = id;
        $scope.zindex =  !$scope.zindex;
        $state.go('comment');
    };
})
.constant('$ionicLoadingConfig', {
    template: '<ion-spinner icon="ios"></ion-spinner> <p>Loading...</p>',
    animation: 'fade-in',
    showBackdrop: false,
    maxWidth: 200,
    showDelay: 0
});