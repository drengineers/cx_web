/**
 * Created by shen on 2017/7/14.
 */
cxApp
.controller('commentController', function($scope,$rootScope, $ionicLoading,$timeout,$http,$state) {
    localStorage.headerTitle = "Comments";
    $rootScope.headerTitle =  localStorage.headerTitle;
    $scope.getComment =function () {
        $ionicLoading.show();
        var url =$rootScope.apiUrl+'/feeds/'+ localStorage.commentId;
        url+='?token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.comment = JSON.stringify(response.data);
            $scope.comment = JSON.parse(localStorage.comment);
            angular.forEach($scope.comment,function (value) {
                var dateCurrentToUTCS =  new Date(new Date().toUTCString()).getTime();
                value.timestamp = new Date(value.timestamp).getTime();
                var time = (dateCurrentToUTCS-value.timestamp);
                value.times = $rootScope.timeConversion(time);
                value.thumbnail = $rootScope.judgeHttp(value.thumbnail);
            });
            $timeout(function () {$ionicLoading.hide();}, 1000);
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {$ionicLoading.hide();}, 1000);
        });
    };
    $scope.doRefresh = function () {
        $scope.getComment();
        $scope.$broadcast('scroll.refreshComplete');
    };
    $scope.getComment();
    $scope.postComment=function () {
        if($('#comment').html()!=''){
            $ionicLoading.show();
            var url =$rootScope.apiUrl+'/feeds/'+ localStorage.commentId;
            url+='?token='+JSON.parse(localStorage.userInfo).token;
            $http({
                method: 'POST',
                url: url,
                data:{username:JSON.parse(localStorage.userInfo).firstName+JSON.parse(localStorage.userInfo).lastName,useremail:localStorage.loginEmail,content:$('#comment').html()},
                headers:{'Content-Type': 'application/json'}
            }).then(function successCallback(response) {

                $rootScope.comeback()

            }, function errorCallback(response) {
                console.log(response.data);
                $timeout(function () {$ionicLoading.hide();}, 1000);
            });
        }
    }
})
.constant('$ionicLoadingConfig', {
    template: '<ion-spinner icon="ios"></ion-spinner> <p>Loading...</p>',
    animation: 'fade-in',
    showBackdrop: false,
    maxWidth: 200,
    showDelay: 0
});