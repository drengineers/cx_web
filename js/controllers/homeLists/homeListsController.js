cxApp
.controller('agendaController', function($scope,$rootScope, $ionicLoading,$timeout,locals,$http,$filter,$ionicSlideBoxDelegate,$state) {
     $ionicLoading.show();
     localStorage.headerTitle ='Agenda';
     $rootScope.headerTitle =  localStorage.headerTitle;
     $scope.dataAll = [];
     $scope.allOrMy = true;
     $scope.num = 0;
     $scope.search='';
     $scope.agendaItemFilter = [];
     $scope.getMyReminder = function () {
        var url =$rootScope.apiUrl+'/getMyReminder/?meetingId='+localStorage.selectmeetingId;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
           $scope.notife=response.data.agendas;
        }, function errorCallback(response) {
            console.log(response.data);
        });
     };
     $scope.getAgenda = function () {
         var url =$rootScope.apiUrl+'/agendas/?meetingId='+localStorage.selectmeetingId;
         url+='&token='+JSON.parse(localStorage.userInfo).token;
         $http({
             method: 'GET',
             url: url,
             headers:{'Content-Type': 'application/json'}
         }).then(function successCallback(response) {
             localStorage.agenda = JSON.stringify(response.data);
             $scope.agendaReminderTurnOn =JSON.parse(localStorage.meetingSelect).agendaReminderTurnOn;
             $scope.agendaul = JSON.parse(localStorage.meetingSelect).includeAgendaTabs;
             $scope.agendaExtension = JSON.parse(localStorage.meetingSelect).agendaExtension;
             console.log(JSON.parse(localStorage.meetingSelect));
             console.log(JSON.parse(localStorage.agenda));
             console.log(moment().tz("America/Dawson").format('Z'));
             $scope.data = response.data;
             console.log(response.data);
             $scope.agendaForEach($scope.data);

         }, function errorCallback(response) {
             console.log(response.data);
             $ionicLoading.hide();
         });
     };
     $scope.agendaForEach = function (data) {
     	 $scope.date = [];
         $scope.day = [];
         angular.forEach(data,function (t) {
             var time = '';
             var timeEnd='';
             var timeStart='';
             var st = new Date(t.startDateTime.replace(/-/g, "/"));
             var et = new Date(t.endDateTime.replace(/-/g, "/"));
             if(JSON.parse(localStorage.meetingSelect).doNotConvertTimezone==false){
                 time = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
                 timeStart = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
                 timeEnd = new Date(et).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
             }else{
                 time = new Date(st).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
                 timeStart = new Date(st).getTime()+ parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
                 timeEnd = new Date(et).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
             }
             var ts = $filter("date")(new Date(time), "dd");
             time =$filter("date")(new Date(time), "EEE,MMM,dd");
             t.time = ts;
             t.timeStart =$filter("date")(new Date(timeStart), 'shortTime');
             t.timeEnd =$filter("date")(new Date(timeEnd), 'shortTime');
             if(t.isGroup==true){
                 angular.forEach(t.items,function (i) {
                     if($.inArray(i.id, $scope.notife)!=-1){
                         i.notify=true;
                     }else{
                         i.notify=false;
                     }
                 });
             }else{
                 if($.inArray(t.id, $scope.notife)!=-1){
                     t.notify=true;
                 }else{
                     t.notify=false;
                 }
             }
             var str =  $scope.date.join(",");
             if(str.indexOf(time)>-1) {}else{$scope.date.push(time) ;}
             var strDay =  $scope.day.join(",");
             if(strDay.indexOf(ts)>-1) {}else{$scope.day.push(ts) ;}
         });
         $timeout(function () {
             $scope.dateArr = [];
             angular.forEach($scope.date,function (v,k) {
                 $scope.dateArr[k]={
                     weekday:v.split(",")[0],
                     month:v.split(",")[1],
                     day:v.split(",")[2]
                 };
             });
             $scope.dataAll=[];
             if( $scope.allOrMy == true){
                 $scope.fullOrMy('all');
             }else{
                 $scope.fullOrMy('my');
             }
             $scope.agendaData =$scope.dataAll[0].data;
             localStorage.agendaData = JSON.stringify($scope.agendaData);
             $scope.num =  $scope.num ? $scope.num:0;
             $scope.timeArr = $scope.dataAll;
         }, 500);
     };
     $scope.getMyReminder();
     $scope.getAgenda();

     $scope.onSlide = function(index) {
         $scope.num = index;
         $scope.agendaData =$scope.dataAll[index].data;
         console.log($scope.num,$scope.dataAll[index].data);
     };
     $scope.nextSlide = function() {
         if($scope.num < $scope.timeArr.length-1){
             $scope.num = $scope.num+1;
             $scope.agendaData =$scope.dataAll[$scope.num].data;
         }
     };
     $scope.preSlide = function() {
         if($scope.num > 0){
             $scope.num = $scope.num-1;
             $scope.agendaData =$scope.dataAll[$scope.num].data;
             console.log($scope.num,$scope.dataAll[$scope.num]);
         }

     };
     $scope.agendaListOpen = function(target,data){
         if($scope.agendaExtension==true){
             $scope.agendaData[target]['open'] = $scope.agendaData[target]['open'] ? false :true;
         }else{
             localStorage.detailData = JSON.stringify(data);
             $state.go('homeLists.myagenda');
         }
     };

     $scope.doRefresh = function() {
        $scope.agendaItemFilter=[];
        $scope.getAgenda();
        $scope.$broadcast('scroll.refreshComplete');
     };

     $scope.yesOpen = function(target,id,num ,itemId,noti){
		 $ionicLoading.show();
         var isAdd=noti==true?0:1;
         var url =$rootScope.apiUrl+'/updateMyReminder/?meetingId='+localStorage.selectmeetingId;
         url+='&token='+JSON.parse(localStorage.userInfo).token;
         $http({
             method: 'POST',
             url: url,
             data:{agendaId:itemId,meetingId:localStorage.selectmeetingId,isAdd:isAdd,useremail:localStorage.loginEmail},
             headers:{'Content-Type': 'application/json'}
         }).then(function successCallback(response) {
             angular.forEach($scope.dataAll[num].data,function (a,n) {
                if(a.id==id){
                    if(a.isGroup==true){
                        angular.forEach(a.items,function (t2,k) {
                            if(t2.id==itemId){
                                $scope.dataAll[num].data[n].items[k].notify = !$scope.agendaData[n].items[k].notify;
                            }
                        });
                    }else{
                        $scope.dataAll[num].data[n].notify = !$scope.agendaData[n]['notify'];
                    }
                }
             });
             $scope.agendaData =  $scope.dataAll[num].data;
             $timeout(function () {
                 $ionicLoading.hide();
             }, 1000);
         }, function errorCallback(response) {
             console.log(response.data);
             $timeout(function () {
                 $ionicLoading.hide();
             }, 1000);
         });
     };

     $scope.openAgenda = function(target,id,num ,itemId,agen){
        $ionicLoading.show();
        console.log('asdfs');
        var isAdd=agen==true?0:1;
        var url =$rootScope.apiUrl+'/updateMyAgenda/?meetingId='+localStorage.selectmeetingId;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'POST',
            url: url,
            data:{agendaId:itemId,meetingId:localStorage.selectmeetingId,isAdd:isAdd,useremail:localStorage.loginEmail},
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            angular.forEach($scope.dataAll[num].data,function (a,n) {
                if(a.id==id){
                    if(a.isGroup==true){
                        var h = 0;
                        angular.forEach(a.items,function (t2,k) {
                            if(t2.id==itemId){
                                $scope.dataAll[num].data[n].items[k].isMyAgenda = !$scope.agendaData[n].items[k].isMyAgenda;

                                if($scope.dataAll[num].data[n].items[k].isMyAgenda==true){
                                    $scope.dataAll[num].data[n].isMyAgenda = true;
                                    if(h=1){
                                        h=0;
                                    }
                                }else{
                                    h=1;
                                }
                            }
                        });

                        if(h==1){
                            $scope.dataAll[num].data[n].isMyAgenda = false;
                        }
                        console.log(789,$scope.dataAll[num].data[n]);
                    }else{
                        $scope.dataAll[num].data[n].isMyAgenda = !$scope.agendaData[n]['isMyAgenda'];
                        if($scope.allOrMy == true){ $scope.dataAll[num].data[n].myagenda==true}else{ $scope.dataAll[num].data[n].myagenda==!$scope.agendaData[n]['myagenda'];}
                        console.log($scope.dataAll[num].data[n]);
                    }
                }
            });
            $scope.titleSearch();
            $scope.agendaData =  $scope.dataAll[num].data;

            console.log($scope.agendaData);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });
    };

     $scope.agendaListOpenOrder = false;
     $scope.rightOrderOpen = false;
     $scope.rightOrder =function (data) {
         if($scope.agendaExtension==true){
             $scope.rightOrderOpen = $scope.rightOrderOpen==true?false:true;
         }else{
             localStorage.detailData = JSON.stringify(data);
             $state.go('homeLists.myagenda');
         }
     };

     $scope.details = function (data) {
         localStorage.detailData = JSON.stringify(data);
         $state.go('homeLists.myagenda');
     };

     $scope.fullOrMy = function (val) {
         $ionicLoading.show();
         $scope.allOrMy = val=='all' ? true : false;
         angular.forEach($scope.day,function (t,k) {
             $scope.dataAll[k] = [];
             $scope.dataAll[k].data=[];
             $scope.dataAll[k].day=[];
             $scope.dataAll[k].myagenda=false;
             angular.forEach($scope.data,function (d) {
                 if(d.time==t){
                     if(d.isGroup!=false){
                         if($scope.allOrMy==true){
                             d.isMyAgenda = true;
                             d.myagenda = true;
                         }else{
                             var x = 0;
                             angular.forEach(d.items,function (t,l) {
                                 if(t.isMyAgenda==true){
                                     d.isMyAgenda = true;
                                     d.myagenda = true;
                                     if(x=1){x=0;}
                                 }else{x=1;}
                             });
                             if(x==1){
                                 d.isMyAgenda = false;
                                 d.myagenda = false;
                             }
                         }

                     }else{
                         if($scope.allOrMy==true || ($scope.allOrMy==false && d.isMyAgenda == true)){
                             d.myagenda = true;
                         }else{
                             d.myagenda = false;
                         }
                     }
                     $scope.dataAll[k].data.push(d);
                     $scope.dataAll[k].myagenda=d.isMyAgenda==true?true:false;
                 }
                 if(d.isGroup!=true){
                     if( JSON.stringify(d.tagItem) != "{}"){
                         $scope.agendaItemFilter.push(d.tagItem);
                     }
                 }
             });
             angular.forEach($scope.dateArr,function (a) {
                 if(a.day==t){
                     $scope.dataAll[k].day.push(a);
                 }
             });
         });
         angular.forEach($scope.dataAll,function (l) {
             angular.forEach(l.data,function (d) {
                 if(d.myagenda==true){
                     l.myagenda=true;
                 }
             });
         });
         $scope.titleSearch();
         $timeout(function () {
             $ionicLoading.hide();
         }, 1000);
     };

     $scope.fiterItem = function (id) {
         if(id=='all'){
             angular.forEach($scope.day,function (t,k) {
                 $scope.dataAll[k] = [];
                 $scope.dataAll[k].data=[];
                 $scope.dataAll[k].day=[];
                 $scope.dataAll[k].myagenda=false;
                 angular.forEach($scope.data,function (d) {
                     console.log(d);
                     if(d.time==t){
                         if(d.isGroup!=false){
                             if($scope.allOrMy==true){
                                 d.isMyAgenda = true;
                                 d.myagenda = true;
                             }else{
                                 var x = 0;
                                 angular.forEach(d.items,function (t,l) {
                                     if(t.isMyAgenda==true){
                                         console.log(t,111);
                                         d.isMyAgenda = true;
                                         d.myagenda = true;
                                         console.log(d, d.myagenda,22);
                                         if(x=1){x=0;}
                                     }else{x=1;}
                                 });
                                 if(x==1){
                                     d.isMyAgenda = false;
                                     d.myagenda = false;
                                 }
                             }

                         }else{
                             if($scope.allOrMy==true || ($scope.allOrMy==false && d.isMyAgenda == true)){
                                 d.myagenda = true;
                             }else{
                                 d.myagenda = false;
                             }
                         }
                         $scope.dataAll[k].data.push(d);
                         $scope.dataAll[k].myagenda=d.isMyAgenda==true?true:false;
                         console.log( $scope.dataAll[k]);
                     }

                 });
                 angular.forEach($scope.dateArr,function (a) {
                     if(a.day==t){
                         $scope.dataAll[k].day.push(a);
                     }
                 });
             });
             angular.forEach($scope.dataAll,function (l) {
                 angular.forEach(l.data,function (d) {
                     if(d.myagenda==true){
                         l.myagenda=true;
                     }
                 });
             });
         }else{
             angular.forEach($scope.day,function (t,k) {
                 $scope.dataAll[k] = [];
                 $scope.dataAll[k].data=[];
                 $scope.dataAll[k].day=[];
                 $scope.dataAll[k].myagenda=false;
                 angular.forEach($scope.data,function (d) {
                     console.log(d);
                     if(d.time==t){
                         if(d.isGroup!=false){
                             if($scope.allOrMy==true){
                                 var gg = 0;
                                 angular.forEach(d.items,function (t,l) {
                                     if(t.tagItem.id==id){
                                         d.isMyAgenda = true;
                                         d.myagenda = true;
                                         console.log(d, d.myagenda,22);
                                         if(gg=1){gg=0;}
                                     }else{gg=1;}
                                 });
                                 if(gg==1){
                                     d.isMyAgenda = false;
                                     d.myagenda = false;
                                 }
                             }
                         }else{
                             if(d.tagItem.id==id){
                                 d.myagenda = true;
                             }else{
                                 d.myagenda = false;
                             }
                         }
                         $scope.dataAll[k].data.push(d);
                         $scope.dataAll[k].myagenda=d.isMyAgenda==true?true:false;
                         console.log( $scope.dataAll[k]);
                     }
                 });
                 angular.forEach($scope.dateArr,function (a) {
                     if(a.day==t){
                         $scope.dataAll[k].day.push(a);
                     }
                 });
             });
             angular.forEach($scope.dataAll,function (l) {
                 angular.forEach(l.data,function (d) {
                     if(d.myagenda==true){
                         l.myagenda=true;
                     }
                 });
             });
         }
         $scope.titleSearch();
         $scope.agendamenu();
     };

     $scope.titleSearch = function () {
         angular.forEach($scope.day,function (t,k) {
             $scope.dataAll[k] = [];
             $scope.dataAll[k].data=[];
             $scope.dataAll[k].day=[];
             $scope.dataAll[k].myagenda=false;
             angular.forEach($scope.data,function (d) {
                 if(d.time==t){
                     if(d.isGroup!=false){
                         if($scope.allOrMy==true){
                             var ss = 0;
                             angular.forEach(d.items,function (t,l) {
                                 if(t.name.toLowerCase().indexOf($scope.search.toLowerCase()) >= 0 ){
                                     d.isMyAgenda = true;
                                     d.myagenda = true;
                                     if(ss=1){ss=0;}
                                 }else{ss=1;}
                             });
                             if(ss==1){
                                 d.isMyAgenda = false;
                                 d.myagenda = false;
                             }
                         }
                     }else{
                         if( $scope.allOrMy == true){
                             if(d.name.toLowerCase().indexOf($('#agendaF').val().toLowerCase()) >= 0){
                                 d.myagenda = true;
                             }else{
                                 d.myagenda = false;
                             }
                         }else{
                             if(d.name.toLowerCase().indexOf($('#agendaF').val().toLowerCase()) >= 0 && d.isMyAgenda==true){
                                 d.myagenda = true;
                             }else{
                                 d.myagenda = false;
                             }
                         }
                     }
                     $scope.dataAll[k].data.push(d);
                     $scope.dataAll[k].myagenda=d.myagenda==true?true:false;
                     // console.log( $scope.dataAll[k]);
                 }
             });
             angular.forEach($scope.dateArr,function (a) {
                 if(a.day==t){
                     $scope.dataAll[k].day.push(a);
                 }
             });
         });
         angular.forEach($scope.dataAll,function (l) {
             angular.forEach(l.data,function (d) {
                 if(d.myagenda==true){
                     l.myagenda=true;
                 }
             });
         });
     };
})

.controller('agendaDetailController',function ($scope,$rootScope, $ionicLoading,$ionicLoadingConfig,$timeout,locals,$http,$filter,$ionicSlideBoxDelegate,$state,$ionicScrollDelegate) {
    $ionicLoading.show();
    localStorage.headerTitle ='My Agenda';
    $rootScope.headerTitle =  localStorage.headerTitle;
    $scope.detailData = JSON.parse(localStorage.detailData);
    $scope.open = false;
    $scope.myagendamore = false;
    $scope.page = 0;
    $scope.mySearch = '';
    $scope.attendaId = $scope.detailData.id;
    angular.forEach($scope.detailData.leaders,function (l) {
        l.image = $rootScope.judgeHttp(l.image);
    });
    $scope.detailData.staticMapImage = $rootScope.judgeHttp( $scope.detailData.staticMapImage);
    $scope.detailData.mapImage =  $scope.detailData.staticMapImage;

    var time = '';
    var timeEnd='';
    var timeStart='';
    var st = new Date($scope.detailData.startDateTime.replace(/-/g, "/"));
    var et = new Date($scope.detailData.endDateTime.replace(/-/g, "/"));
    if(JSON.parse(localStorage.meetingSelect).doNotConvertTimezone==false){
        time = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
        timeStart = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
        timeEnd = new Date(et).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
    }else{
        time = new Date(st).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
        timeStart = new Date(st).getTime()+ parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
        timeEnd = new Date(et).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
    }

    if($scope.detailData.timeStart == $scope.detailData.timeEnd){
        $scope.detailData.TT = $filter("date")(new Date(timeStart), "MMMM d, yyyy")+' starting at '+$scope.detailData.timeStart;
    }else{
        $scope.detailData.TT = $filter("date")(new Date(timeStart), "MMMM d, yyyy")+' from '+$scope.detailData.timeStart+ ' to '+$scope.detailData.timeEnd;
    }

    console.log($scope.detailData);

    $scope.loadAttendData = function (index,keyword) {
        $ionicLoading.show();
        var url =$rootScope.apiUrl+'/attendees/?meetingId='+localStorage.selectmeetingId+'&agendaId='+$scope.attendaId;
        if(index){url+='&index='+index;}
        if(keyword){url+='&keyword='+keyword;}
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            angular.forEach(response.data.attendees, function(value){
                value.image = $rootScope.judgeHttp(value.image);
            });
            console.log(response.data.attendees);
            $scope.attenda = response.data.attendees;
            $timeout(function () {
                $scope.open = true;
                $ionicLoading.hide();
            }, 1000);
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });

    };

    $scope.loadMore=function (tag) {
        console.log('444');
        // console.log($scope.myagendamore,tag);
        // $ionicLoading.show();
        // $scope.page = $scope.page+1;
        // console.log( $scope.page);
        // $scope.loadAttendData();
    };

    $scope.agendaGoProfile =function (id) {
        console.log(JSON.parse(localStorage.userInfo).userId);
        $state.go('profile',{id:id});
    };

    $scope.myagendaSearch = function () {
        $scope.loadAttendData(0,$('#mySearch').val());
    };

    $scope.goEcontent = function (data) {
        localStorage.myagedaEcontent = JSON.stringify(data);
        localStorage.eContentType = 1;
        $state.go('homeLists.econtent');
    };

    $scope.goSurvey =function (val) {
        window.open(val);
    };

    $scope.agendatoMap =function () {
        localStorage.travelMap = JSON.stringify($scope.detailData);
        $state.go('map');
    };

    $scope.goProfile =function (id) {
        console.log(JSON.parse(localStorage.userInfo).userId);
        $state.go('profile',{id:id});
    };

    $timeout(function () {
        $ionicLoading.hide();
    }, 1000);

})
.constant('$ionicLoadingConfig', {
    template: '<ion-spinner icon="ios"></ion-spinner> <p>Loading...</p>',
    animation: 'fade-in',
    showBackdrop: false,
    maxWidth: 200,
    showDelay: 0
})

.controller('attendeesController', function($scope,$rootScope, $ionicLoading,$timeout,$http,$filter,$state) {

 	$rootScope.headerTitle = "Attendees";
 	$scope.companyOpen = false;
    $scope.tagsOpen = false;
    $scope.hasmore = false;
    $scope.page = 0;
 	$ionicLoading.show();
	$scope.attendeesCompany = function () {
		var company = $rootScope.apiUrl+'/attendee-companies/?meetingId='+localStorage.selectmeetingId+'&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: company,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.attendeesCompany = JSON.stringify(response.data);
            console.log(JSON.parse(localStorage.attendeesCompany),1);
            $scope.attendeesC=JSON.parse(localStorage.attendeesCompany);
            if($scope.attendeesC.length > 0){
                $scope.companyOpen = true;
			}
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    $scope.attendeesTags = function () {
        var tags = $rootScope.apiUrl+'/attendee-tags/?meetingId='+localStorage.selectmeetingId+'&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: tags,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.attendeesTags = JSON.stringify(response.data);
            console.log(JSON.parse(localStorage.attendeesTags));
            $scope.attendeesT=JSON.parse(localStorage.attendeesTags);
            if($scope.attendeesT.length > 0){
                $scope.tagsOpen = true;
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

 	$scope.attendeesData = function (index,pageSize,keyword,tag,company,region,inputType) {
		var url =$rootScope.apiUrl+'/attendees/?meetingId='+localStorage.selectmeetingId;
 		if(index){
			url+='&index='+index;
		}
        if(pageSize){
            url+='&pageSize='+pageSize;
        }
		if(keyword){
            url+='&keyword='+keyword;
		}
		if(tag){
            url+='&tag='+tag;
        }
        if(company){
            url+='&company='+company;
        }
        if(region){
            url+='&region='+region;
        }
        if(inputType){
            url+='&inputType='+inputType;
        }
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            angular.forEach(response.data.attendees, function(value){
                value.image = $rootScope.judgeHttp(value.image);
            });
            if(response.data.attendees.length < response.data.pageSize){
                $scope.hasmore = false;
            }else{
                $scope.hasmore = true;
            }
            localStorage.attendees = JSON.stringify(response.data);
            console.log(JSON.parse(localStorage.attendees));
            $scope.attendeesD=JSON.parse(localStorage.attendees).attendees;
            console.log($scope.attendeesD);
        }, function errorCallback(response) {
            console.log(response.data);
        });

    };

    $scope.attendeesCompany();
    $scope.attendeesTags();
    $scope.attendeesData(0);
    $timeout(function () {$ionicLoading.hide();}, 2000);
    $scope.companyChange =function (val) {
        $ionicLoading.show();
        if(val=='All Companies'){
            $scope.attendeesD = JSON.parse(localStorage.attendees).attendees;
		}else{
            $scope.attendeesD=$filter("filter")(JSON.parse(localStorage.attendees).attendees,document.getElementById("companyChange").value);
		}

        $timeout(function () {$ionicLoading.hide();}, 2000);
    };
    $scope.tagsChange =function (val) {
        $ionicLoading.show();
        if(val=='All Tags'){
            $scope.attendeesD = JSON.parse(localStorage.attendees).attendees;
        }else{
            $scope.attendeesD=$filter("filter")(JSON.parse(localStorage.attendees).attendees,document.getElementById("tagsChange").value);
        }

        $timeout(function () {$ionicLoading.hide();}, 2000);
    };
    $scope.attendeesSearch =function () {
		var search =document.getElementById('attendeesSearch');
        $scope.attendeesData(0,20,search.value);
    };

	$scope.goProfile =function (id) {
		console.log(JSON.parse(localStorage.userInfo).userId);
		$state.go('profile',{id:id});
    };

    $scope.doRefresh = function(tag) {
        $scope.attendeesData(0);
        $scope.$broadcast('scroll.refreshComplete');
    };
    $scope.loadMore=function (tag) {
        $ionicLoading.show();
        $scope.page = $scope.page+1;
        $scope.attendeesData($scope.page);
    };
})

.controller('attendees_emailController', function($scope,$rootScope, $ionicLoading,$timeout) {
 	 $rootScope.headerTitle = "Attendees";
 	 $ionicLoading.show();
	$timeout(function () {$ionicLoading.hide();}, 1000);
})

.controller('econtentController', function($scope,$rootScope, $ionicLoading,$timeout,$http,$state,$filter) {
     $ionicLoading.show();
     if($rootScope.version!='HPE'){localStorage.headerTitle = "eContent";}else{localStorage.headerTitle = "Content";}
 	 $rootScope.headerTitle = localStorage.headerTitle;
	 $scope.getEContent =function(index){
         $scope.econtentTag = [];
         var url =$rootScope.apiUrl+'/econtent/?meetingId='+localStorage.selectmeetingId;
         if(index){url +='&index='+index;}
         url+='&token='+JSON.parse(localStorage.userInfo).token;
         $http({
             method: 'GET',
             url: url,
             headers:{'Content-Type': 'application/json'}
         }).then(function successCallback(response) {
             angular.forEach(response.data, function(value){
                 value.link = $rootScope.judgeHttp(value.link);
                 angular.forEach(value.tags,function(v){
                     if( $scope.econtentTag.length > 0){
                         angular.forEach($scope.econtentTag,function(t){
                             if(v.id == t.id){} else{$scope.econtentTag.push(v);}
                         });
                     }else{
                         $scope.econtentTag.push(v);
                     }
                 });
             });
             $timeout(function () {
                 localStorage.econtent = JSON.stringify(response.data);
                 $scope.econtentD=JSON.parse(localStorage.econtent);
                 console.log($scope.econtentD,$scope.econtentTag);
             }, 500);
             $timeout(function () {$ionicLoading.hide();}, 1000);
         }, function errorCallback(response) {
             console.log(response.data);
             $timeout(function () {$ionicLoading.hide();}, 1000);
         });
	 };

     $scope.tagsChange =function (val) {
        $ionicLoading.show();
        if(val=='All Tags'){
            $scope.econtentD=JSON.parse(localStorage.econtent);
        }else{
            $scope.econtentD = [];
            angular.forEach(JSON.parse(localStorage.econtent), function(value){
                value.link = $rootScope.judgeHttp(value.link);
                angular.forEach(value.tags,function(v){
                    if(v.tag==val){
                        $scope.econtentD.push(value);
                    }
                });
            });
        }
        $timeout(function () {$ionicLoading.hide();}, 1000);
    };

     $scope.newLink = function (url) {
         localStorage.iframeTitle = 'econtent';
         localStorage.iframeUrl = url;
         $state.go('eject');
     };

     if(localStorage.eContentType && localStorage.eContentType==1){
         $scope.econtentTag = [];
         console.log(JSON.parse(localStorage.myagedaEcontent),0);
         var arr = JSON.parse(localStorage.myagedaEcontent);
         angular.forEach(arr, function(value){
             console.log(value.link);
             value.link = $rootScope.judgeHttp(value.link);
             console.log($rootScope.judgeHttp(value.link));
             angular.forEach(value.tags,function(v){
                 if( $scope.econtentTag.length > 0){
                     angular.forEach($scope.econtentTag,function(t){
                         if(v.id == t.id){} else{$scope.econtentTag.push(v);}
                     });
                 }else{
                     $scope.econtentTag.push(v);
                 }
             });
         });
         $timeout(function () {$scope.econtentD=arr;$ionicLoading.hide();}, 1000);
     }else{
         $scope.getEContent(0);
     }

     $scope.esearch = function () {
         $ionicLoading.show();
         $timeout(function () {
             $scope.econtentD=[];
             var search =document.getElementById('econtentSearch');
             if(localStorage.eContentType && localStorage.eContentType==1){
                 angular.forEach(JSON.parse(localStorage.myagedaEcontent), function(value){
                     if(value.title.toLowerCase().indexOf(search.value.toLowerCase()) >= 0) {
                         $scope.econtentD.push(value);
                     }
                 });
             }else{
                 angular.forEach(JSON.parse(localStorage.econtent), function(value){
                     if(value.title.toLowerCase().indexOf(search.value.toLowerCase()) >= 0) {
                         $scope.econtentD.push(value);
                     }
                 });
             }
         	 $ionicLoading.hide();
         }, 1000);
     };

     $scope.doRefresh = function(tag) {
        $scope.getEContent(0);
        $scope.$broadcast('scroll.refreshComplete');
     };
     $scope.loadMore=function (tag) {
        console.log($scope.hasmore,tag);
        $ionicLoading.show();
        $scope.page = $scope.page+1;
        console.log( $scope.page);
        $scope.getEContent($scope.page);
     };
})
.controller('travelLogisticsController', function($scope,$rootScope, $ionicLoading,$timeout,$http,$state) {
    $ionicLoading.show();
   	$rootScope.headerTitle =  localStorage.headerTitle;
    $scope.sj=[];
   	$scope.getTravel =function () {
          var url =$rootScope.apiUrl+'/travels/?meetingId='+localStorage.selectmeetingId;
          url+='&token='+JSON.parse(localStorage.userInfo).token;
          $http({
             method: 'GET',
             url: url,
             headers:{'Content-Type': 'application/json'}
          }).then(function successCallback(response) {
         	 console.log(response.data);
             localStorage.travel = JSON.stringify(response.data);
         	 $scope.travel = JSON.parse(localStorage.travel);
         	 if($rootScope.version == 'HPE'){
                 $scope.sj = [
                     {
                         'id':'1',
                         'title':'Maps',
                         'data':$scope.travel.maps
                     },
                     {
                         'id':'2',
                         'title':'Dining',
                         'data':$scope.travel.dining
                     },
                     {
                         'id':'3',
                         'title':'Hotels',
                         'data':$scope.travel.hotels
                     }
                 ];
             }else{
                 $scope.sj = [
                     {
                         'id':'1',
                         'title':'Maps',
                         'data':$scope.travel.maps
                     },
                     {
                         'id':'2',
                         'title':'Ding&Hotels',
                         'data':$scope.travel.hotels
                     },
                     {
                         'id':'3',
                         'title':'Activities',
                         'data':$scope.travel.activity
                     }
                 ];
             }

             $scope.sj[0].type=1;
             $scope.sj[1].type=0;
             $scope.sj[2].type=0;

              if(localStorage.headerTitle=='Travel & Logistics'){

              }else if(localStorage.headerTitle=='Dining' || localStorage.headerTitle=='Ding&Hotels'){
                  $timeout(function () {
                      console.log($scope.sj);
                      $scope.switch($scope.sj[1].id);
                  },300);
              }
             $ionicLoading.hide();
          }, function errorCallback(response) {
             console.log(response.data);
             $ionicLoading.hide();
          });
     };

    $scope.switch =function(id){
        console.log(id);
        $ionicLoading.show();
        $scope.sj[0].type=0;
        $scope.sj[1].type=0;
        $scope.sj[2].type=0;
        $scope.sj[id-1].type=1;
        localStorage.headerTitle = $scope.sj[id-1].title;
        $rootScope.headerTitle = localStorage.headerTitle;
        $ionicLoading.hide();
    };


    $scope.toMap =function (data) {
        localStorage.travelMap = JSON.stringify(data);
        $state.go('map');
     };
    $scope.doRefresh = function() {
        $scope.getTravel();
        $scope.$broadcast('scroll.refreshComplete');
    };
    $scope.getTravel();
})
.controller('wayfidingController', function($scope,$rootScope, $ionicLoading,$timeout) {
 	 $rootScope.headerTitle = "Wayfiding";
 	 $ionicLoading.show();
	 $timeout(function () {$ionicLoading.hide();}, 1000);
})
.controller('notificationsController', function($scope,$rootScope,$stateParams, $ionicLoading,$timeout,$cookieStore,$filter,$http,$ionicScrollDelegate,$state) {
    if($rootScope.version!='HPE'){
        $scope.type=[
            {
                title:'Notifications',
            },
            {
                title:'Surveys',
            },
            {
                title:'Polls',
            }
        ];
    }else{
        $scope.type=[
            {
                title:'Notifications',
            },
            {
                title:'Feedback',
            },
            {
                title:'Polls',
            }
        ];
    }

    // if(localStorage.isNotifi){}else{localStorage.isNotifi=1;}

    if(localStorage.notificationIsRead){
        $scope.c = JSON.parse(localStorage.notificationIsRead);
    }else{
        $scope.c =[];
        localStorage.notificationIsRead = JSON.stringify($scope.c);
    }
    $scope.isReadData =function () {
		if(localStorage.headerTitle=='Surveys' || localStorage.headerTitle=='Feedback'){
            $scope.allSurveys.forEach(function (t) {
                if(JSON.parse(localStorage.notificationIsRead).join(",").indexOf(t.id) > -1){
                    t.isRead = false;
                }else{
                    t.isRead = true;
                }
            });
		}else{
            $scope.allNotifictionData.forEach(function (t) {
                if(JSON.parse(localStorage.notificationIsRead).join(",").indexOf(t.id) > -1){
                    t.isRead = false;
                }else{
                    t.isRead = true;
                }
                t.time =$filter("date")(new Date(t.sentDateTime), "EEE dd,yyyy hh:mm a");
                // $scope.date = $filter('date')(new Date(t.sentDateTime),"yyyy-MM-dd hh:mm:ss EEEE");
            });
		}
    };
    $scope.allNotify =function () {
        var url =$rootScope.apiUrl+'/notifications/?meetingId='+localStorage.selectmeetingId;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.notification = JSON.stringify(response.data);

            $scope.allNotifictionData =JSON.parse(localStorage.notification);
            $scope.isReadData();
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };
    $scope.read =function (data,id,link) {
        console.log(data);
        var str =  JSON.parse(localStorage.notificationIsRead).join(",");
        if(str.indexOf(id)>-1) {}else{$scope.c.push(id);}
        localStorage.notificationIsRead = JSON.stringify($scope.c);
        $scope.isReadData();
        if(!data.linkType && data.surveylink!=''&& data.completed==0){
            window.open(data.surveylink);
        }
        if(data.linkType=='p_home'){
            $state.go('index');
        }
        if(data.linkType=='p_agenda'){
            $state.go('homeLists.agenda');
        }
        if(data.linkType=='p_agenda_detail'){
            $scope.getAgenda(data.linkTo);
        }
        if(data.linkType=='p_econtent'){
            $state.go('homeLists.econtent');
        }
        if(data.linkType=='p_travel_logistics'){
            if(data.linkTo=='maps'){
                localStorage.headerTitle = 'Travel & Logistics';
            }else{
                if($rootScope.version=='HPE'){
                    localStorage.headerTitle = 'Dining';
                }else{
                    localStorage.headerTitle = 'Ding&Hotels';
                }
            }
            $state.go('homeLists.travelLogistics');
        }
        if(data.linkType=='p_attendees_detail'){
            $state.go('profile',{id:data.linkTo});
        }
        if(data.linkType=='p_surveys'){
            if(data.completed && data.completed==1){
                $rootScope.showAlertNotification();
            }else if(data.completed && data.completed==0){
                window.open(data.linkTo);
            }
        }
        if(data.linkType=='p_notifications_surveys'){
            if($rootScope.version=='HPE'){
                $scope.notificationBanner('Feedback');
            }else{
                $scope.notificationBanner('Surveys');
            }
        }
        if(data.linkType=='p_notifications_polls'){
            angular.forEach($scope.allNotifictionData,function (v) {
                if(data.linkTo == v.linkTo){
                    localStorage.headerTitle = "Poll";
                    $rootScope.headerTitle = localStorage.headerTitle;
                    $scope.getPoll(v.linkTo,v.id);
                }
            });
        }
        if(data.linkType=='externalUrl' || data.linkType=='externalUrlInBrowser'){
            window.open(data.linkTo);
        }
    };
    $scope.getAgenda = function (id) {
        var url =$rootScope.apiUrl+'/agendas/?meetingId='+localStorage.selectmeetingId;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            $scope.agendaReminderTurnOn =JSON.parse(localStorage.meetingSelect).agendaReminderTurnOn;
            $scope.agendaul = JSON.parse(localStorage.meetingSelect).includeAgendaTabs;
            $scope.agendaExtension = JSON.parse(localStorage.meetingSelect).agendaExtension;
            console.log(JSON.parse(localStorage.agenda));
            console.log(moment().tz("America/Dawson").format('Z'));
            $scope.AgendaData = response.data;
            console.log(response.data);
            angular.forEach($scope.AgendaData,function (v) {
                if(v.id == id){
                    var time = '';
                    var timeEnd='';
                    var timeStart='';
                    var st = new Date(v.startDateTime.replace(/-/g, "/"));
                    var et = new Date(v.endDateTime.replace(/-/g, "/"));
                    if(JSON.parse(localStorage.meetingSelect).doNotConvertTimezone==false){
                        time = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
                        timeStart = new Date(st).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
                        timeEnd = new Date(et).getTime() - new Date().getTimezoneOffset() * 60 * 1000;
                    }else{
                        time = new Date(st).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
                        timeStart = new Date(st).getTime()+ parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
                        timeEnd = new Date(et).getTime() + parseInt(moment().tz(JSON.parse(localStorage.meetingSelect).timezone).format('Z')) * 3600 * 1000;
                    }
                    v.timeStart =$filter("date")(new Date(timeStart), 'shortTime');
                    v.timeEnd =$filter("date")(new Date(timeEnd), 'shortTime');
                    if(v.timeStart == v.timeEnd){
                       v.TT = $filter("date")(new Date(timeStart), "MMMM d, yyyy")+' starting at '+v.timeStart;
                    }else{
                       v.TT = $filter("date")(new Date(timeStart), "MMMM d, yyyy")+' from '+v.timeStart+ ' to '+v.timeEnd;
                    }
                    localStorage.detailData= JSON.stringify(v);
                    $state.go('homeLists.myagenda');
                }
            });
        }, function errorCallback(response) {
            console.log(response.data);
            $ionicLoading.hide();
        });
    };
    $scope.notificationBanner =function (title) {
        $ionicLoading.show();
        $ionicScrollDelegate.scrollTop([true]);
        localStorage.headerTitle =title;
        $rootScope.headerTitle = localStorage.headerTitle;

        if(localStorage.headerTitle == 'Surveys' || localStorage.headerTitle == 'Feedback' ){
            localStorage.isNotifi=0;
            $scope.getAllSurveys();
        }
        if(localStorage.headerTitle =='Notifications' ){
            localStorage.isNotifi=1;
            $scope.allNotify();
        }
        if(localStorage.headerTitle =='Polls'){
            localStorage.isNotifi=2;
        }
        $timeout(function () {$ionicLoading.hide();}, 1000);
    };
    $rootScope.$on("Callgetnotification",function(){
        $scope.notificationBanner($rootScope.headerTitle);
    });
    $scope.getAllSurveys = function () {
        var url =$rootScope.apiUrl+'/surveys/?meetingId='+localStorage.selectmeetingId+'&enableNotification=1';
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            // var notificationD = JSON.parse(localStorage.notification);
            // notificationD.forEach(function (t) {
				// if(t.linkType=='p_surveys'){
				// 	var child ={
				// 		id:t.id,
            //             name:t.name,
            //             surveylink:t.linkTo
				// 	};
				// }
            // });
            localStorage.surveys = JSON.stringify(response.data);
            $scope.allSurveys = JSON.parse(localStorage.surveys);
            $scope.isReadData();
            console.log($scope.allSurveys);
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    $scope.getPoll =function (id,nid) {

        console.log($rootScope.headerTitle);
        localStorage.pollId = id;
        var url =$rootScope.apiUrl+'/poll/'+id+'?useremail='+localStorage.loginEmail;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.pollData = JSON.stringify(response.data);
            $scope.read(nid);
            localStorage.headerTitle = "Poll";
            $rootScope.headerTitle = localStorage.headerTitle;
            if(JSON.parse(localStorage.pollData).result=='submit'){
                localStorage.pollJudgment=0;
                $state.go('poll');
            }else{
                localStorage.pollResults = localStorage.pollData;
                localStorage.pollJudgment=1;
                $state.go('poll-results');
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    if( localStorage.isNotifi==1){
        localStorage.headerTitle ='Notifications';
        $rootScope.headerTitle = localStorage.headerTitle;
        $scope.allNotify();
    }else if(localStorage.isNotifi==0){
        if($rootScope.version=='HPE'){
            localStorage.headerTitle ='Feedback';
        }else{
            localStorage.headerTitle ='Surveys';
        }
        $rootScope.headerTitle = localStorage.headerTitle;
        $scope.getAllSurveys();
    }else{
        localStorage.headerTitle = "Polls";
        $rootScope.headerTitle = localStorage.headerTitle;
        $scope.allNotify();
        $scope.notificationBanner($rootScope.headerTitle);
    }
    $scope.doRefresh = function() {
        $rootScope.headerTitle = localStorage.headerTitle;
        if(localStorage.headerTitle =='Surveys' || localStorage.headerTitle =='Feedback'){
            $scope.getAllSurveys();
        }else if(localStorage.headerTitle =='Poll' ){
            localStorage.headerTitle = "Polls";
            $rootScope.headerTitle = localStorage.headerTitle;
            $scope.allNotify();
            $scope.notificationBanner($rootScope.headerTitle);
        }else {
            $scope.allNotify();
        }
        $scope.$broadcast('scroll.refreshComplete');
    };

 	$ionicLoading.show();
	$timeout(function () {$ionicLoading.hide();}, 1000);
})

.controller('welcomeController', function($scope,$rootScope, $ionicLoading,$timeout, $state, $stateParams,$cookieStore) {
// 	 $ionicLoading.show();
	$timeout(function () {
		$(".powered").show();
		$(".welcome-cisco-logo").show();
	}, 1000);

    // var info = $cookieStore.get('userInfo');
	if(localStorage.userInfo){
        $timeout(function () {
            $state.go('lists');
        }, 2000);
	}else{
        $timeout(function () {
            $state.go('login');
        }, 2000);
	}


});
