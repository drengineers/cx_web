cxApp
.controller('pollController', function($scope,$rootScope,$state, $ionicLoading,$timeout,$stateParams,$http) {
    $ionicLoading.show();
    localStorage.headerTitle = "Poll";
    $rootScope.headerTitle = localStorage.headerTitle;
    $scope.resultsPoll =function(results){
        localStorage.headerTitle = "Poll";
        $rootScope.headerTitle = localStorage.headerTitle;
        // $rootScope.labels = ["Green", "Peach", "Grey"];
        $rootScope.color = [ '#ABC234','#B8B9BD', '#62BA12'];
        $rootScope.data = [];
        // $rootScope.pollData = {
        //     "id": "56388a6e7ca178a12390795c",
        //     "completionImage": "completion.jpg",
        //     "submitMessage": "Thanks for completing the poll! Here's what others think:",
        //     "question": "who is the most valueable player on the San Jose Earthquakes this year?",
        //     "answers": [
        //         {"answer": "Chris Woodolowski", "rate": "75%"},
        //         {"answer": "Shea Salinas", "rate": "14%"},
        //         {"answer": "Matias Perez Garcia", "rate": "11%"}
        //     ]
        // };
        if(localStorage.pollJudgment==1){
            $rootScope.pollData = JSON.parse(localStorage.pollResults);
        }else{
            $rootScope.pollData = JSON.parse(localStorage.pollData);
        }
        $rootScope.pollData.answers.forEach(function (t) {
            t.num =parseInt(t.rate);
            $rootScope.data.push(t.num);
        });
        $timeout(function () {
            localStorage.headerTitle = "Poll";
            $rootScope.headerTitle = localStorage.headerTitle;
            // $state.go('poll-results');
        }, 1000);
    };

    $scope.pollD =function () {
        var pollId = localStorage.pollId;
        var url =$rootScope.apiUrl+'/poll/'+pollId+'?useremail='+localStorage.loginEmail;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.pollData = JSON.stringify(response.data);
            if(JSON.parse(localStorage.pollData).result=='submit'){
                $scope.pdata = JSON.parse(localStorage.pollData);
                localStorage.pollJudgment=0;
            }else{
                localStorage.pollResults = localStorage.pollData;
                localStorage.pollJudgment=1;
                $scope.resultsPoll(localStorage.pollJudgment);
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
    };

    $timeout(function () {$ionicLoading.hide();}, 1000);

    $scope.reFresh= function () {
        $ionicLoading.show();
        $timeout(function () {
            $scope.pollD();
            $ionicLoading.hide();
        }, 1000);
    };

    $scope.pollD();
});