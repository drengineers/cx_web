cxApp
.controller('mapController',function($scope,$rootScope, $ionicLoading,$timeout,$ionicActionSheet,$state) {
    localStorage.headerTitle = "Map";
    $rootScope.headerTitle =  localStorage.headerTitle;
    $scope.mapData = JSON.parse(localStorage.travelMap);
    console.log(JSON.parse(localStorage.travelMap));
    $scope.mapData.mapImage = $rootScope.judgeHttp($scope.mapData.mapImage);
    console.log($scope.mapData);
    $scope.mapTo =function (url) {
        localStorage.mapIframe=url;
        $state.go('mapto');
    };

})
.controller('maptoController',function($scope,$rootScope, $ionicLoading,$timeout,$ionicActionSheet,$compile,$sce) {
    // script(src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCqJJsV4x1VItVWDwG3yyr0zu8ATAwMzgw');
    // $scope.maphide = true;
    // $ionicLoading.show();
    // $timeout(function () {
		// $ionicLoading.hide();
		// $scope.maphide = false;
    // }, 1000);

    // $scope.init = function() {
    //     var myLatlng = new google.maps.LatLng(41.85,-87.65);
    //     var indianapolis =  new google.maps.LatLng(39.79,-86.14);
    //     var mapOptions = {
    //         center: myLatlng,
    //         zoom: 7,
    //         mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     };
    //     var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    //     var directionsDisplay = new google.maps.DirectionsRenderer({
    //         map: map
    //     });
    //     // Set destination, origin and travel mode.
    //     var request = {
    //         destination: indianapolis,
    //         origin: myLatlng,
    //         travelMode: 'DRIVING'
    //     };
    //
    //     // Pass the directions request to the directions service.
    //     var directionsService = new google.maps.DirectionsService();
    //     directionsService.route(request, function(response, status) {
    //         if (status == 'OK') {
    //             // Display the route on the map.
    //             directionsDisplay.setDirections(response);
    //         }
    //     });
    //     //Marker + infowindow + angularjs compiled ng-click
    //     // var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";
    //     // var compiled = $compile(contentString)($scope);
    //     //
    //     // var infowindow = new google.maps.InfoWindow({
    //     //     content: compiled[0]
    //     // });
    //
    //     var marker = new google.maps.Marker({
    //         position: myLatlng,
    //         map: map,
    //         title: 'Uluru (Ayers Rock)'
    //     });
    //
    //     google.maps.event.addListener(marker, 'click', function() {
    //         infowindow.open(map,marker);
    //     });
    //
    //     $scope.map = map;
    // };
    //
    // $scope.centerOnMe = function() {
    //     $ionicLoading.show();
    //     if(!$scope.map) {
    //         return;
    //     }
    //
    //
    //
    //     // $scope.loading = $ionicLoading.show({
    //     //     content: 'Getting current location...',
    //     //     showBackdrop: false
    //     // });
    //
    //     navigator.geolocation.getCurrentPosition(function(pos) {
    //         $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    //
    //     }, function(error) {
    //         alert('Unable to get location: ' + error.message);
    //     });
    //     $timeout(function () {
    //         $ionicLoading.hide();
    //     }, 1000);
    // };
    //
    // $scope.clickTest = function() {
    //     alert('Example of infowindow with ng-click')
    // };
    $scope.urlSrc = $sce.trustAsResourceUrl(localStorage.mapIframe);
});
