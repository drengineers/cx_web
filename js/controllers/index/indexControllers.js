cxApp
.controller( 'indexController',function($scope,$rootScope,$timeout,$http,$state,$stateParams,$ionicLoading){
	$scope.open = false;
	$scope.hidemodeldiv = false;
    $scope.footer_toggle = function(){
        $scope.open=!$scope.open;
        $scope.zindex =  !$scope.zindex;
    };
    $scope.headeropen = false;
    $scope.header_toggle = function(){
        $scope.headeropen=!$scope.headeropen;
         if($scope.headeropen){
			 $scope.hidemodeldiv = true;
		 }else{
			 $scope.hidemodeldiv = false;
		 }
    };
	
	$scope.hidemodel = function(){
		$scope.hidemodeldiv = false;
		if($scope.headeropen){
			$scope.headeropen = false;
		}
	};
	$scope.header_hidemodel=function(){
        $scope.headeropen = false;
        $scope.open=false;
        $scope.hidemodeldiv = false;
        console.log(JSON.parse(localStorage.userInfo).userId);
        $state.go('profile',{id:JSON.parse(localStorage.userInfo).userId});
	};
	
    
    $scope.open_footer = function(){
        $scope.zindex =  !$scope.zindex;
       	$scope.header_toggle();
		$scope.open=true;

    };
    
    $scope.close_footer = function(){
    	$scope.header_toggle();
		$scope.open=false;
    };
    
    $scope.myagenda=function(){
		$scope.headeropen = false;
		$scope.open=false;
		$scope.hidemodeldiv = false;
		$state.go('homeLists.agenda');
    };
    
    $scope.attendees=function(){
        $scope.headeropen = false;
        $scope.open=false;
        $scope.hidemodeldiv = false;
        $state.go('homeLists.attendees');
    };
    
    $scope.econtent=function(){
        localStorage.eContentType = 0;
        $scope.headeropen = false;
        $scope.open=false;
        $scope.hidemodeldiv = false;
        $state.go('homeLists.econtent');
    };
    $scope.travelLogistics=function(){
        localStorage.headerTitle ='Travel & Logistics';
	    $state.go('homeLists.travelLogistics');
    };
    $scope.surveys = function(){
        localStorage.isNotifi = 0;
        $scope.headeropen = false;
        $scope.open=false;
        $scope.hidemodeldiv = false;

        if(localStorage.headerTitle =='Polls' || localStorage.headerTitle =='Notifications'){
            if($rootScope.version=='HPE'){
                localStorage.headerTitle ='Feedback';
            }else{
                localStorage.headerTitle ='Surveys';
            }
            $rootScope.headerTitle = localStorage.headerTitle;
            $rootScope.$emit("Callgetnotification",function(){});
        }else{
            if($rootScope.version=='HPE'){
                localStorage.headerTitle ='Feedback';
            }else{
                localStorage.headerTitle ='Surveys';
            }
            $rootScope.headerTitle = localStorage.headerTitle;
            $state.go('homeLists.notifications');
        }


    };
    $scope.goNotifications = function(){
        $scope.headeropen = false;
        $scope.open=false;
        $scope.hidemodeldiv = false;
        localStorage.isNotifi = 1;
        if(localStorage.headerTitle =='Polls' || localStorage.headerTitle =='Feedback' || localStorage.headerTitle =='Surveys'){
            localStorage.headerTitle ='Notifications';
            $rootScope.headerTitle = localStorage.headerTitle;
            $rootScope.$emit("Callgetnotification",function(){});
            // $rootScope.$emit("CallgetallNotify",{});
        }else{
            localStorage.headerTitle ='Notifications';
            $rootScope.headerTitle = localStorage.headerTitle;
            $state.go('homeLists.notifications');
        }

    };
    $scope.notifications=function(){
        localStorage.isNotifi = 2;
    		$scope.headeropen = false;
	    	$scope.open=false;
	    	$scope.hidemodeldiv = false;
	    	if(localStorage.headerTitle =='Notifications' || localStorage.headerTitle =='Feedback' || localStorage.headerTitle =='Surveys'){
                localStorage.headerTitle ='Polls';
                $rootScope.headerTitle = localStorage.headerTitle;
                $rootScope.$emit("Callgetnotification",function(){});
            }else{
                localStorage.headerTitle ='Polls';
                $rootScope.headerTitle = localStorage.headerTitle;
                $state.go('homeLists.notifications');
			}



    };
    
    $scope.meeting = function(){
    		$scope.headeropen = false;
	    	$scope.open=false;
	    	$scope.hidemodeldiv = false;
	    	$state.go('lists');	
    };
    
    $scope.zindex = false;
    $scope.wayfidingopen = false;
 	$scope.wayfiding = function(){
 		 $ionicLoading.show();
 		 $scope.headeropen = false;
 		 $scope.hidemodeldiv = false;
		 $timeout(function () {
			 $ionicLoading.hide();
			 $timeout(function () {
				 $scope.zindex = true;
			 }, 400);
    			 $scope.wayfidingopen = true;
		 }, 1000);
    		
    };
 	
 	$scope.wayfidingclose = function(){
 		$scope.zindex = false;
    		$scope.wayfidingopen = false;
 	};
 	
    $scope.messageopen = false;
 	$scope.message = function(){
 		 $scope.headeropen = false;
 		 $scope.hidemodeldiv = false;
			$ionicLoading.hide();
			$timeout(function () {
				$scope.zindex = true;
			}, 400);
    			$scope.messageopen = true;

    };
 	
 	$scope.messageclose = function(){
 		$scope.zindex = false;
    		$scope.messageopen = false;
 	};
 	
 	$scope.pull = function(){
		$scope.modelopen = true;
		$scope.upmenuopen = true;
		$scope.zindex = true;
	};
 	$scope.modelclose = function(){
		$scope.modelopen = false;
		$scope.upmenuopen = false;
		$timeout(function () {
				$scope.zindex = false;
		}, 400);
	};
 	
 	
 	$scope.profile = function(){
 			$scope.headeropen = false;
 	};
//  $scope.$on('$ionicView.beforeEnter',function(){  
//          commonFactory.backUrl($location.url());   
//  }) 
    $scope.agendamenuanimation = false;
    $scope.toback = false;
    $scope.agendamenu = function(){
        if($scope.agendamenuanimation){

            $scope.agendamenuanimation = false;
            $scope.toback = false;
            $timeout(function () {
                $scope.zindex = false;
            }, 400);
        }else{

            $scope.agendamenuanimation = true;
            $timeout(function () {
                $scope.toback = true;
            }, 200);
            $scope.zindex = true;
        }
    };
    $scope.agendamodel = function(){
        $timeout(function () {
            $scope.zindex = false;
        }, 400);
        if($scope.agendamenuanimation){
            $scope.agendamenuanimation = false;
                $scope.toback = false;

        }else{
            $scope.agendamenuanimation = true;
            $timeout(function () {
                $scope.toback = true;
            }, 200);
        }
    };
   
});





