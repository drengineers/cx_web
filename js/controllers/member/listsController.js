cxApp
.controller('listsController', function($scope,cache,$rootScope,$http,$state, $stateParams,$ionicLoading,$timeout,$cookieStore) {
    localStorage.headerTitle = $rootScope.version=='HPE'?"My Briefings":"My Meetings/Events/Communities";
    $rootScope.headerTitle = localStorage.headerTitle;
    $scope.meeting =function () {
        $http({
            method: 'GET',
            url: $rootScope.apiUrl+'/venues/?token='+JSON.parse(localStorage.userInfo).token,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            localStorage.meetingAll = JSON.stringify(response.data);
            $rootScope.meetingData = JSON.parse(localStorage.meetingAll);
            $ionicLoading.hide();
        }, function errorCallback(response) {
            console.log(response.data);
            $ionicLoading.hide();
        });
    };

 	$scope.lists_toggle = function(target){
		if($scope.meetingData[target]['open']){
			$scope.meetingData[target]['open']=false;
		}else{
			$scope.meetingData[target]['open']=true;
		}
	};
 	$scope.lists_deail = function(id){
        localStorage.selectmeetingId = id;
        localStorage.showRightMenu = true;
        $rootScope.showRightMenu =localStorage.showRightMenu;
        $state.go('index');
 	};

    $timeout(function () {
        $ionicLoading.show();
        $scope.meeting();

    }, 500);
});
