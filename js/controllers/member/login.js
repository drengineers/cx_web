/**
 * Created by shen on 2017/6/6.
 */
cxApp.controller("loginEmailController",["$scope","$state","$rootScope","$ionicLoading","$timeout","$http","$cookieStore", function($scope,$state,$rootScope, $ionicLoading,$timeout,$http,$cookieStore) {
    $scope.emailError = false;
    $scope.loginMesssage = 'Step 1:Please enter your email address';
    $scope.loginMesssageError = '';
    $scope.First = function () {
        $ionicLoading.show();
        var loginEmail = document.getElementById('loginEmail');
        if(loginEmail.value!=''){
            $http({
                method: 'POST',
                url: $rootScope.apiUrl+'/check-email/',
                data:{email:loginEmail.value},
                headers:{'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                $scope.emailError = false;
                localStorage.loginEmail = loginEmail.value;
                // $cookieStore.put('loginEmail',loginEmail.value);
                $state.go('loginPassword');
            }, function errorCallback(response) {
                $scope.emailError = true;
                $scope.loginMesssageError = "We're sorry,  but it appears that your email address is not registererd.  Please try again or contact your meeting/event organizer for assistance.";
            });
        }else{
            $scope.emailError = true;
            $scope.loginMesssageError = "Please enter your email address";
        }
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    };

}]);

cxApp.controller("loginPasswordController",["$scope","$state","$rootScope","$ionicLoading","$timeout","$http","$cookieStore", function($scope,$state,$rootScope, $ionicLoading,$timeout,$http,$cookieStore) {
    $scope.passwordError = false;
    $scope.loginSecondMesssage = 'Step 2:Please enter your password';
    $scope.loginSecondMesssageError = '';
    $scope.Second = function () {
        $ionicLoading.show();
        var loginPassword = document.getElementById('loginPassword');
        if(loginPassword.value!=''){
            $http({
                method: 'POST',
                url: $rootScope.apiUrl+'/authenticate/',
                data:{username:localStorage.loginEmail,password:loginPassword.value},
                headers:{'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                if(response.data.result!='failed'){
                    $scope.passwordError = false;
                    localStorage.password = loginPassword.value;
                    localStorage.userInfo = JSON.stringify(response.data);
                    localStorage.isPassWord = true;
                    localStorage.showRightMenu = true;
                    $rootScope.showRightMenu =localStorage.showRightMenu;
                    $timeout(function () { $state.go('lists');}, 500);

                }else{
                    $scope.passwordError = true;
                    $scope.loginSecondMesssageError = "Incorrect Password. Please try again.";
                }
            }, function errorCallback(response) {
                console.log(response);
            });
        }else{
            $scope.passwordError = true;
            $scope.loginSecondMesssageError = "Please enter your password";
        }
        $timeout(function () {$ionicLoading.hide();}, 1000);
    };

    $scope.goupdatepassword =function () {
        $state.go('updatepassword');
    };

}]);