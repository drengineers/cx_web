cxApp
.controller('updatepasswordController', function($scope,$rootScope, $ionicLoading,$timeout,$state,$http) {
    $ionicLoading.show();
    localStorage.headerTitle ="Update Password";
    $rootScope.headerTitle = localStorage.headerTitle;
    $scope.infoEmail=localStorage.loginEmail;
    // if(localStorage.userInfo){
    //     $scope.showRightMenu = true;
    // }else{
    //     $scope.showRightMenu = false;
    // }
    console.log($scope.showRightMenu);
    $scope.tpPassword=function () {
        var url =$rootScope.apiUrl+'/forgotPassword/';
        // url+='&token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'POST',
            url: url,
            data:{email:$scope.infoEmail},
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            console.log(response.data);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, function errorCallback(response) {
            console.log(response);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });
    };
    $scope.tpPassword();
});