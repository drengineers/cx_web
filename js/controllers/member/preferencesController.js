cxApp
.controller('preferencesController', function($scope,$rootScope, $ionicLoading,$timeout,$state,$http) {
	localStorage.headerTitle ="Preferences";
    $rootScope.headerTitle = localStorage.headerTitle;
    $scope.infoEmail=localStorage.loginEmail;
    $scope.jtSHow=false;
    $scope.data = JSON.parse(localStorage.myAttendees);
    $scope.data.image=$rootScope.judgeHttp($scope.data.image);
    // $scope.myCroppedImage =  $scope.data.image;
    console.log($rootScope.headerTitle ,JSON.parse(localStorage.myAttendees), $scope.data);
    $ionicLoading.show();
	$scope.updatemypassword = function(){
		$ionicLoading.show();
		$timeout(function () {
			$ionicLoading.hide();
		}, 1000);
		$state.go('updatepassword');
	};

    $scope.getProfile = function () {
        var url =$rootScope.apiUrl+'/profile/?';
        url+='token='+JSON.parse(localStorage.userInfo).token;
        $ionicLoading.show();
        $http({
            method: 'GET',
            url: url,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            console.log(response.data.firstName);
            $scope.data.firstName = response.data.firstName;
            $scope.data.lastName = response.data.lastName;
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, function errorCallback(response) {
            console.log(response);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });
    };

	$scope.bio = function(id){
        localStorage.bio =  $scope.data.bio;
        localStorage.headerTitle ="Bio";
        localStorage.bioId = id;
        $rootScope.headerTitle = localStorage.headerTitle;
        $('.header-main .header').hide();
        $('.preferences-Ancestors').hide();
        $('.bio-Ancestors').show();
	};

    $scope.save_changes_bottom = function(){
        $ionicLoading.show();
        localStorage.headerTitle ="Preferences";
        $rootScope.headerTitle = localStorage.headerTitle;
        $scope.data.bio = $("#bioData").html();

        var url =$rootScope.apiUrl+'/update-profile/';
        url+='?token='+JSON.parse(localStorage.userInfo).token;
        var img = $('#myself').attr("src");
        var fdStart = img.indexOf("data");
        $scope.data.isPhoneShown = $('#numberCheckBox').is(':checked')?true:false;
        $scope.data.isEmailDisappear = $('#emailCheckBox').is(':checked')?true:false;
        $scope.data.isLinkedInShown = $('#linkedInCheckBox').is(':checked')?true:false;
        $scope.data.receiveNotifications = $('#notificationsCheckBox').is(':checked')?true:false;
        $scope.data.notifyComments = $('#commentsCheckBox').is(':checked')?true:false;
        $scope.data.notifyLikes = $('#likesCheckBox').is(':checked')?true:false;
        var data={
            userId:$scope.data.id,
            firstName:$scope.data.firstName,
            lastName:$scope.data.lastName,
            company:$scope.data.company,
            title:$scope.data.title,
            phoneNumber:$scope.data.phoneNumber,
            bio:$scope.data.bio,
            isPhoneShown:$scope.data.isPhoneShown,
            allowEmail:$scope.data.isEmailDisappear,
            linkedInURL:$scope.data.linkedInURL,
            receiveNotifications:$scope.data.receiveNotifications,
            notifyComments:$scope.data.notifyComments,
            notifyLikes:$scope.data.notifyLikes,
            isLinkedInShown:  $scope.data.isLinkedInShown
        };
        if(fdStart == 0){data.imageData=img;}
        $http({
            method: 'POST',
            url: url,
            data:data,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            console.log(response);
            $('.bio-Ancestors').hide();
            $('.header-main .header').show();
            $('.preferences-Ancestors').show();
            $timeout(function () {$ionicLoading.hide();}, 1000);
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {$ionicLoading.hide();}, 1000);
        });
    };
    $scope.go_backPreferences = function(){
        localStorage.headerTitle ="Preferences";
        $rootScope.headerTitle = localStorage.headerTitle;
        $('.bio-Ancestors').hide();
        $('.header-main .header').show();
        $('.preferences-Ancestors').show();
    };

    $scope.postSave =function () {
        $ionicLoading.show();
        var url =$rootScope.apiUrl+'/update-profile/';
        url+='?token='+JSON.parse(localStorage.userInfo).token;
        var img = $('#myself').attr("src");
        var fdStart = img.indexOf("data");
        $scope.data.isPhoneShown = $('#numberCheckBox').is(':checked')?true:false;
        $scope.data.isEmailDisappear = $('#emailCheckBox').is(':checked')?true:false;
        $scope.data.isLinkedInShown = $('#linkedInCheckBox').is(':checked')?true:false;
        $scope.data.receiveNotifications = $('#notificationsCheckBox').is(':checked')?true:false;
        $scope.data.notifyComments = $('#commentsCheckBox').is(':checked')?true:false;
        $scope.data.notifyLikes = $('#likesCheckBox').is(':checked')?true:false;
        var data={
            userId:$scope.data.id,
            firstName:$scope.data.firstName,
            lastName:$scope.data.lastName,
            company:$scope.data.company,
            title:$scope.data.title,
            phoneNumber:$scope.data.phoneNumber,
            bio:$scope.data.bio,
            isPhoneShown:$scope.data.isPhoneShown,
            allowEmail:$scope.data.isEmailDisappear,
            linkedInURL:$scope.data.linkedInURL,
            receiveNotifications:$scope.data.receiveNotifications,
            notifyComments:$scope.data.notifyComments,
            notifyLikes:$scope.data.notifyLikes,
            isLinkedInShown:  $scope.data.isLinkedInShown
        };
        if(fdStart == 0){data.imageData=img;}
        $http({
            method: 'POST',
            url: url,
            data:data,
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            $rootScope.showAlert();
            $state.go('profile',{id:$scope.data.id});
            $timeout(function () {$ionicLoading.hide();}, 1000);
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {$ionicLoading.hide();}, 1000);
        });
    };

    $scope.getProfile();

    // var handleFileSelect=function(evt) {
    //     $scope.jtSHow=true;
    //     console.log($scope.jtSHow);
    //     var file=evt.currentTarget.files[0];
    //     var reader = new FileReader();
    //     reader.onload = function (evt) {
    //         $scope.$apply(function($scope){
    //             $scope.myImage=evt.target.result;
    //         });
    //     };
    //     reader.readAsDataURL(file);
    // };
    //
    // angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

    $scope.takePicture = function () {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA
        };

        // udpate camera image directive
        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.cameraimage = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log('Failed because: ');
            console.log(err);
        });
    };

	$timeout(function () {
        $ionicLoading.hide();
    }, 1000);

});