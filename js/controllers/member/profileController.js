cxApp
.controller('profileController',function($scope,$state,$rootScope, $ionicLoading,$timeout,$stateParams,$http) {
    $ionicLoading.show();
    $timeout(function () {$ionicLoading.hide();}, 1000);
    $scope.myself = false;
    var id =$stateParams.id;
    if(id == JSON.parse(localStorage.userInfo).userId){
        $rootScope.headerTitle = "My Profile";
        $scope.myself = true;
    }else{
        $rootScope.headerTitle = "Attendees";
	}

	$scope.getAttendessMy=function () {
        var url =$rootScope.apiUrl+'/attendees/?attendeeId='+id;
        url+='&token='+JSON.parse(localStorage.userInfo).token;
        $ionicLoading.show();
		$http({
			method: 'GET',
            url: url,
			headers:{'Content-Type': 'application/json'}
		}).then(function successCallback(response) {
            console.log(response.data);
            localStorage.myAttendees = JSON.stringify(response.data);
            $scope.data = JSON.parse(localStorage.myAttendees);
            $scope.data.image=$rootScope.judgeHttp($scope.data.image);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
		}, function errorCallback(response) {
			console.log(response);
            $timeout(function () {
                $ionicLoading.hide();
            }, 1000);
		});
    };
    $scope.getAttendessMy();
    // angular.forEach(JSON.parse(localStorage.attendees).attendees, function(value){
    // 	console.log(value.id);
    //     if(id==value.id){
		// 	$scope.data = value;
		// }
    // });
	console.log($scope.data);

	$scope.toPreferences =function () {
		$state.go('preferences');
    };

	$scope.newmessageheader = "New Message";
	$scope.headeropen = false;

	$scope.modelopen = false;
	$scope.upmenuopen = false;
	
	$scope.modelnoclickopen = false;
	$scope.scalemenuopen = false;
	
	$scope.messageCancel = function(){
		$scope.modelopen = true;
		$scope.upmenuopen = true;
	};
	$scope.messagesendclick = function(){
		$scope.modelnoclickopen = true;
		$scope.scalemenuopen = true;
	};
	$scope.scalemenuclose= function(){
		$scope.modelnoclickopen = false;
		$scope.scalemenuopen = false;
	};
	
	$scope.modelclose = function(){
		$scope.modelopen = false;
		$scope.upmenuopen = false;
	};
	$scope.deleteDraft = function(){
			$timeout(function () {
				$scope.messageclose();
			}, 500);
	};
	
	$scope.subject = function(){
	
	};
	$scope.messagesend = false;
	$scope.messagefocus = function(){
		$scope.messagesend = true;	
	};

	
	
	
});