cxApp
.controller('footerController', ['$scope', '$sce','$rootScope','$ionicLoading','$ionicLoadingConfig','$timeout','$ionicScrollDelegate','$http',function($scope,$sce,$rootScope,$ionicLoading,$ionicLoadingConfig,$timeout,$ionicScrollDelegate,$http) {



	$scope.addActive = function () {
		$scope.feed=[
			{
				username: "e-cosmetology",
				max: 50,
				_id: "s779ysr2nlth9r2acrjaxysy",
				isShow: true,
				target: "None",
				likers: [ ],
				userTitle: null,
				type: "twitter",
				attendeeId: "",
				useremail: "",
				comments: [ ],
				thumbnail: "https://pbs.twimg.com/profile_images/688714089699258368/QJzjY2Xx_normal.jpg",
				content: "#BTL #cardiology #BTL_cardiology #ABPM #�������� https://t.co/Md3Jc9ntCf... https://t.co/zcdbwLqi5j https://t.co/KMuLVw6Z4E",
				assetUrl: "https://pbs.twimg.com/media/DEnvKcBXkAACJ_m.jpg",
				timestamp: "2017-07-13 14:10:47",
				numLike: 0,
				targetUrl: "",
				include: true,
				meeting: "rzpn11at754z3bdktexol8p7",
				numComment: 0
			},
			{
				username: "Nirav Shah",
				max: 5,
				_id: "h5vo0rsoet5f8eyg1usnmkuq",
				isShow: true,
				target: "",
				likers: [
					"andrea_susman@designreactor.com"
				],
				userTitle: "PM",
				type: "uMessage",
				attendeeId: "dskd48ngk8oymgtrdbw1ossk",
				useremail: "nirav_shah@designreactor.com",
				comments: [
					{
						username: "Leon Papkoff",
						useremail: "leon_papkoff@designreactor.com",
						timestamp: "2017-06-14 15:56:07",
						attendeeId: "frwrohsilzol6m7rj8a05xqt",
						content: "Sounds good, I'll be onsite at 12pm to prep. What time are you getting there?",
						_id: "agsucpk6bggsakosrjw9mxfx",
						thumbnail: "20170602010117.png"
					},
					{
						username: "Nirav Shah",
						useremail: "nirav_shah@designreactor.com",
						timestamp: "2017-06-14 20:11:53",
						attendeeId: "dskd48ngk8oymgtrdbw1ossk",
						content: "I'll be at the CXC around 1",
						_id: "2prifx9dicm44nce3v8r26im",
						thumbnail: "20170606071113_20170606071103_rlfsongvqe.png"
					}
				],
				thumbnail: "20170606071113_20170606071103_rlfsongvqe.png",
				content: "Looking forward to the open house ? at the John T Chamber CXC today! ",
				assetUrl: "",
				timestamp: "2017-06-14 15:46:17",
				numLike: 1,
				targetUrl: "",
				include: true,
				meeting: "8b2or6hppa6d6i63sxaru00h",
				numComment: 2
			},
		];
		angular.forEach($scope.feed, function(value){
			value.timestamp = new Date(value.timestamp).valueOf();
			var time = (dateCurrentToUTCS-value.timestamp);
			value.times = $rootScope.timeConversion(time);
			value.thumbnail = $rootScope.judgeHttp(value.thumbnail);
			value.content = $rootScope.urlReplace(value.content);
			if(value.comments.length != 0){
				angular.forEach(value.comments, function(value){
					value.thumbnail = $rootScope.judgeHttp(value.thumbnail);
				});
			}
            value.isLike = false;
            value.likers.forEach(function (t) {
                if(t == $rootScope.email){
                    value.isLike = true;
                }
            })
		});
		$ionicLoading.show();
		$ionicScrollDelegate.scrollTop([true]);
		$timeout(function () {
			$ionicLoading.hide();
		}, 1000);
	};
	$scope.addLike = function(id){
        $scope.feed.forEach(function (t) {
        	if(t._id == id){
        		t.likers.push($rootScope.email);
        		t.numLike = t.numLike + 1;
        		t.isLike = true;
			}
		})
	};
    $scope.lessLike = function(id){
        $scope.feed.forEach(function (t) {
            if(t._id == id){
            	var arrLike = [];
                t.likers.forEach(function (t2) {
					if(t2 != $rootScope.email){
                        arrLike.push(t2);
					}
				});
                t.likers = arrLike;
                t.numLike = t.numLike - 1;
                t.isLike = false;
            }
        })
    };
    $scope.hasmore = true;
    $scope.page = 0;
    $scope.doRefresh = function() {
        $scope.getAllFeed(0);
        $scope.$broadcast('scroll.refreshComplete');
    };



}])
.constant('$ionicLoadingConfig', {
    template: '<ion-spinner icon="ios"></ion-spinner> <p>Loading...</p>',
    animation: 'fade-in',
    showBackdrop: false,
    maxWidth: 200,
    showDelay: 0
});
