cxApp
.controller('releaseController',function($scope,$rootScope, $ionicLoading,$timeout,$ionicActionSheet,$http,$state) {

    localStorage.headerTitle = "Add A New Post";
    $rootScope.headerTitle =  localStorage.headerTitle;
    if($rootScope.version=='HPE'){
        $scope.imageUrlNow='image/HPE/activityStream/07_hpe_activity_stream_add_post_@2x.png';
    }else{
        $scope.imageUrlNow='image/footer/AEBC_AddAPost5417_07_@2x.png';
    }

    // $rootScope.headerBlockOrNone = !$rootScope.headerBlockOrNone;
    // $scope.readURL = function(input) {
    //     console.log('fas');
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function (e) {
    //             $scope.imageUrlNow = e.target.result;
    //             // $('#blah').attr('src', e.target.result);
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // };
    $scope.releasePost =function () {
        var val = document.getElementById("leave-message-textarea").innerText;
        var img = $('#releaseImg').attr("src");
        var fdStart = img.indexOf("data");
        var type = 'uMessage';
        if(fdStart == 0){
            type='uImage';
        }
        console.log(JSON.parse(localStorage.userInfo));
        console.log(JSON.parse(localStorage.meetingSelect));
        $ionicLoading.show();
        var url =$rootScope.apiUrl+'/feeds/';
        url+='?token='+JSON.parse(localStorage.userInfo).token;
        $http({
            method: 'POST',
            url: url,
            data:{type:type,meeting:JSON.parse(localStorage.meetingSelect).id,content:val,username:JSON.parse(localStorage.userInfo).firstName+''+JSON.parse(localStorage.userInfo).lastName,useremail:localStorage.loginEmail,thumbnail:JSON.parse(localStorage.userInfo).image,userTitle:JSON.parse(localStorage.userInfo).title,imageData:img},
            headers:{'Content-Type': 'application/json'}
        }).then(function successCallback(response) {
            console.log(response);
            $timeout(function () {$ionicLoading.hide();}, 1000);
            $state.go('index');
        }, function errorCallback(response) {
            console.log(response.data);
            $timeout(function () {$ionicLoading.hide();}, 1000);
        });
    }

});