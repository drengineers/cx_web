angular.module('starter.services', ['ionic'])
.factory('utilService', function(ionicToast) {
    return {
        //toast plugin api doc: http://market.ionic.io/plugins/ionictoast
        toast:function(msg){
            ionicToast.show(msg, 'top', false, 2500);
        },
        showToast:function(msg){
            ionicToast.show(msg, 'top', true, 2500);
        },
        hideToast:function(){
            ionicToast.hide();
        }
    }
}) 